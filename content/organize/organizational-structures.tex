\chapter{Organizational Structures}
\label{ch:organizational_structures}

\epigraph{We live in capitalism, its power seems inescapable -- but then, so did the divine right of kings.
Any human power can be resisted and changed by human beings.}
{Ursula K. Le Guin\supercite{le-guin-speech}}

\index{organizing|(}

\noindent
Medics are an important part of the broader infrastructure that enables effective political action and civil unrest.
Medics, along with legal support, organizers, material donors, and others, allow individuals to engage in actions without having to be experts in every domain or procure every resource on their own.
Comrades can risk arrest knowing that a legal team is there to get them out of jail and help minimize the State's punitive measures.
The presence of medics allows individuals and groups to take risky actions that may lead to bodily harm knowing that someone will be there to care for them if they are injured.

In order to operate effectively, it is important that medics organize themselves within the community in which they operate.
This means familiarizing yourself with the existing organizations that carry out political actions and provide mutual aid.

\section*{Non-Hierarchical Organizing}

No matter how you organize and who you work with, as an anarchist you should adhere to the principles of non-hierarchical organizing.
Medics and those they work with have different domains of knowledge, different education (both formal and informal), and different levels of experience.
This is especially true for medics who may be working on teams with doctors who have never been to a riot or protesters who only know the basics of first aid they've picked up on their own.\footnotemark[1]

\footnotetext[1]{
\index{medics!terminology@terminology for|(}
As an aside, use of ``first-aider'' as a term for medic is discouraged because it creates an unnecessary differentiation between medics who are professional or highly certified and medics who are not.
To the public, it can also give the impression that medics are a haphazard assembly of individuals who only minimally know what they are doing.
Use of the term ``medic'' as the general classifier for those who provide medical care during actions is suggested.
\index{medics!terminology@terminology for|)}
}

Non-hierarchical organizing means that there is no decision making hierarchy when it comes to organizing, planning, and acting during mobilizations.
It is important to come to \gls{consensus}\index{consensus} for both long-term planning and what will be done at any given action.
\index{autonomy!personal|(}%
Even during an action, medics must remember that they are free to choose how to act, as are others.
If someone becomes uncomfortable with a plan or the situation, they are free to choose to take a course of action that may not fit in with what was previously decided.
You should neither give nor obey orders.
\index{autonomy!personal|)}

However, for administering care, especially in emergency situations, the ranking of skill level, experience, and certifications\index{certifications} must be respected.
Additionally, decision-making protocols should be structured in a predictable way.
Knowing who the primary care giver is and who the secondaries are when handling patients prevents valuable time from being wasted.
That said, it is important to remember that less experienced medics may still be able to see something more experienced medics don't.

The protocol you use for handling a patient may be the first to arrive is the primary care giver until overridden by someone with more advanced medical knowledge.
The presence or absence of overrides should be clearly established before attending an action.

\section*{Riot Medics Within Emergency Medicine}
\index{emergency medical services|(}

As a riot medic, you are part of the larger emergency medical care system operating in your city.
You may need to consult local laws to understand when you can be relieved of duty by other medical professionals such as Emergency Medical Services (EMS) personnel arriving on the scene.
Riot medicine inherently means operating under conditions that are adverse to administering prompt and ideal care for patients.
There may be police blockades\index{police!interference with medics} that do not allow ambulances through, or EMS movements may be disrupted by your comrades' actions or those of your opponents.
Evacuations for critically injured patients may be necessary, and it best to ensure that cooperative interaction with traditional EMS is not jeopardized by repeated friction with riot medics.
Failing to adhere to standard protocols for emergency medicine may cause problems or delays during future interactions.
A scenario that should be avoided is an unwillingness to work together between traditional medical personnel and riot medics.
This may endanger patients' lives.

Such failures tend to be rare, and you may find that traditional EMS has the opposite reaction to the work done by medics.
They may be willing to support you even if they cannot do so officially, either by making their ambulances available for rapid response during large actions or by providing material support.

\index{emergency medical services|)}

\section*{The Buddy Pair}

\index{buddy pairs|(}

While acting as a medic, you may work as part of a medic collective, be embedded in an affinity group\index{affinity group}, or work as an unaffiliated individual.
No matter how you choose to operate, you must always work with a buddy.
You may have different buddies during different actions, and you may shuffle buddies with other medics during a given action as changing situations may require.
Your buddy does not have to be a medic themself, and they may simply be a comrade you can pair up with at actions.

\begin{figure}[htb]
\centering
\caption{Buddies\supercite{baedr}}
\includesvg[height=7cm, keepaspectratio]{buddy-pair}
\end{figure}

Medics should not operate alone.
This, like many other directives about how to operate, is not ironclad, but is a guideline that is most always applicable.
However, as someone who may be new to riot medicine or street protests in general, it is inadvisable for you to operate without a buddy.

The buddy pair is the fundamental organizational unit for medics.
You and your buddy are responsible for looking after each other's well-being (both physical and mental) before, during, and after an action.
A medic does not separate from their buddy during an action with the exception of multiple patients in close proximity requiring care.
You cannot take care of your buddy, and they cannot watch out for you when you are not together.
Separating endangers both parties.

Some common responsibilities you and your buddy have toward each other include:

\begin{itemize}
    \item Being a second set of eyes while scanning for patients or danger
    \item Providing a second perspective or opinion for a situation (medical or otherwise)
    \item Being the devil's advocate or voice of caution
    \item Dividing equipment and carrying redundant equipment
    \item Double checking equipment
    \item Reminding the other to eat and drink
    \item Being alert while the other rests
    \item Controlling a crowd while the other assists a patient
    \item Preventing people from taking photos or video of a patient
    \item Being a secondary for two-person CPR
    \item Assisting with moving or carrying a patient
    \item Communicating with EMS, other medics, or other groups while the other is helping a patient
    \item Debriefing each other at the end of the day
    \item Supporting each other's mental health in the long-term
\end{itemize}

\index{buddy pairs|)}

\section*{As an Individual Medic}

\index{medics!operating alone|(}

If there are no existent medic collectives and there are no groups with medics operating in your area, you may choose to start your venture into riot medicine as an individual.
This may be an option you find appealing even if there are existing groups as there is no organizational overhead for grabbing a bag and going to demonstrations when you feel like it.

Whatever the reason is for operating on your own, you should nevertheless make connections and contacts with existing groups and comrades in your area.
New faces are expected at public events, but there is some amount of implied authority and trust that goes with medics, especially if they are uniformed.
Anarchist and left-wing movements must always be cautious against informants\index{informants}, and your presence without coordination may arouse suspicion.
For more on informants, see \fullref{ch:general_tactics}.

Connecting with local organizations and activists will also let them know that you are a resource that can be relied upon for actions.
If you are predictable, your presence can encourage people to come out to actions, and it can create a willingness for comrades to take action they otherwise might not.

The disadvantage of working alone is that there is no one to pick up slack if you are unable to attend actions, especially if you have made a commitment to support one.
You may also feel pressured to attend all actions to prevent any one from being without medical support.
Both of these can lead to burnout\index{burnout}, especially if you do not have other medics to debrief with after actions.

\index{medics!operating alone|)}

\section*{As Part of a Collective}

\index{medics!operating in a collective|(}

Organizing as part of a \gls{collective} comes with many advantages over operating alone.
You will work with and get to know others with different experiences with whom you can share skills.
Working with a group diffuses the obligation to attend every action across many individuals.
Caring for traumatized and brutalized patients can take an emotional toll on individual medics, and having a supportive group where you can discuss these events with others will help reduce emotional stress.

\index{donations|(}
As some \glspl{collective} become more established, they may choose to formalize themselves with the State and register as a nonprofit organization\index{nonprofit organizations} in order to solicit donations.
Even without the legal status of a nonprofit, the existence of a named, organized collective can encourage comrades to support you financially.
Equipment and restocking supplies can be expensive, and your community may be able to help you.
\index{donations|)}

\index{anonymity|seealso {operational security}}
\index{anonymity!medics@of medics|(}
Some collectives choose to remain more nameless and may simple be known as ``the local medics.''
Before you establish yourself as a public group with a recognizable name, you may want to consider whether name recognition within your community is worth the associated risks of name recognition by fascists and the State.
You should also be cautious of joining a group simply because they already \textit{do} have a name.
Notoriety does not necessarily imply actual utility within the community.
Be useful, not important.
\index{anonymity!medics@of medics|)}

\index{medics!operating in a collective|)}

\section*{With Other Individuals and Collectives}

\index{working groups|(}

For larger actions and mobilizations, there may be many medics groups from surrounding regions operating together to provide medical support.
These medics may be individuals, or they belong to collectives or affinity groups.
For a single action or short mobilization, many groups and individuals working together as a temporary unit is called a working group.

Working groups will pool knowledge and come to agreements on how to best serve the community given their different skill levels, comfort zones, and tactics.
Some medics may want to embed themselves in the Bloc, and others may be more risk averse and wish to be clearly marked and on the sidelines.
Medics who cannot be as physically active may want to set up water and first-aid stations for actions.
The working group will consider everyone's abilities and goals and find a way to allow the different individuals and collectives to operate symbiotically while maximizing available medical support for protesters or demonstrators.

Even for smaller actions, you should be aware of other medics in your region.
Check in with them before actions to see if they will attend, and during actions make sure you're aware of each other's presence.
Piling all the medics at the front of a large march might not the best use of resources, and you may want to coordinate to get better coverage.

\index{working groups|)}

\section*{Bylaws, Codes of Conduct, and Consensus Statements}

\index{working groups|(}
\index{medic collectives|(}

\index{codes of conduct|(}
\index{bylaws|(}

Long-running collectives and temporary working groups may want to consider adopting bylaws and a \acrlong{CoC} (\acrshort{CoC}).
Bylaws define the responsibilities an individual has toward a collective or working group.
Bylaws include things like requiring attending a certain percent collective meetings or how consensus can be reached by the collective.
A \acrshort{CoC} defines the responsibilities an individual has towards members of the collective, working group, and community.
\acrshort{CoC}s include things like statements against harassment, discrimination, and abuse.\footnotemark[2]

% TODO it may be appropriate to mention Conflict is Not Abuse here

\index{bylaws|)}

\footnotetext[2]{
The Tor Project\index{Tor Project} has a robust \acrshort{CoC}\index{codes of conduct}\supercite{tor-coc} (archive: \url{https://archive.is/IiyG6}).
Another example is the Citizen Code of Conduct\supercite{citizen-coc} (archive: \url{https://archive.is/7VS6s}).
}

Collectives are encouraged to spend some time developing their own \acrshort{CoC} as it helps the group define themselves and establish shared ethics.
A \acrshort{CoC} is also a means of ensuring inclusion by stating that harassment or willful insensitivity toward someone's identity are not tolerated.\footnotemark[3]
Working groups are shorter lived, and it may not be worth spending time to define a bespoke \acrshort{CoC}.
Use of a template may be preferred.

\index{codes of conduct|)}

\footnotetext[3]{
Some opponents of \acrshort{CoC}s claim that they are a purely US phenomenon and that exporting them to the rest of the world is some sort of US-left-hegemony over the sovereignty of non-US left movements.
These same people tend to think that being an asshole is ok because it's ``freedom of expression'' or that others need to toughen up.
People like that can fuck off.
Be gentle with each other so we can be dangerous together.
}

\index{points of unity|see {consensus statements}}
\index{consensus statements|(}

Because different collectives and individuals may have different sets of tactics and ethics, a medic working group may want to develop a consensus statement in preparation for a large action.\footnotemark[4]
This consensus statement is a formal declaration of intent of action as well as a political stance, and it is made public ahead of the action.
Such a statement may include specifics like affirming that the medics will not collaborate with police.
Consensus statements may also choose to define medics as neutral parties and request that all medics refrain from displays of political alignment, or they may define medics as active members of the action.\footnotemark[5]
Coming to consensus helps prevent post facto arguments over individuals' actions, and it gives the public a sense of what to expect from medical support.

\index{consensus statements|)}

\footnotetext[4]{
Examples of street medic consensus statements\index{consensus statements} for a mass action can be found on the NYC Action Medical\index{NYC Action Medical} website\supercite{nyc-action-medical-about} (listed as ``Points of Unity'') and the Ende Gelände\index{Ende Gelände} website\supercite{ende-gelaende-medic-consensus} (archive: \url{https://archive.is/l7HRh}).
}

\footnotetext[5]{
For a full discussion of neutral vs. partisan medics, see \fullref{ch:general_tactics}.
}

\index{working groups|)}
\index{medic collectives|)}

\subsection*{The St. Paul Principles}

\index{police!repression|see {repression}}
\index{tactics!anti-repression|seealso {operational security}}
\index{tactics!anti-repression|seealso {St. Paul Principles}}

\index{St. Paul Principles|(}

The St. Paul Principles\footnotemark[6] are a set of guidelines for political organizing in the face of political repression\index{repression}.
They may be used on their own as part of a consensus statement, or you may find them useful for adding to a \acrshort{CoC}.
The principles are listed in their original form below:\supercite{st-paul-principles}

\footnotetext[6]{
The St. Paul Principles came out of the political resistance to the Republican National Convention\index{Republican National Convention} (RNC) in St. Paul, Minnesota in 2008.
The principles were agreed upon by the various groups that were organizing together in order to maximize their collective impact.
These principles have endured in anarchist and non-anarchist circles in part because they effectively counter State disruption of organizing.
}

\index{tactics!diversity@diversity of|see {diversity of tactics}}

\begin{enumerate}
    \item Our solidarity will be based on respect for a diversity of tactics\index{diversity of tactics} and the plans of other groups.
    \item The actions and tactics used will be organized to maintain a separation of time or space.
    \item Any debates or criticisms will stay internal to the movement, avoiding any public or media denunciations of fellow activists and events.
    \item We oppose any state repression of dissent, including surveillance, infiltration\index{infiltrators!St. Paul Principles@in St. Paul Principles}, disruption, and violence.
          We agree not to assist law enforcement actions against activists and others.
\end{enumerate}

Regardless of how you organize, or what type of actions you attend, the St. Paul Principles are a good guideline for considering how you operate with other groups and individuals.
Different individuals and groups will be willing and able to take different kinds of actions, and everyone's actions can affect the safety and amount of repression faced by others.
A full discussion of tactics is covered in \fullref{pt:tactics}.

\index{diversity of tactics|(}
\triplesubsection{Principle 1}
The first principle is to respect diversity of tactics.
Some medics may not be comfortable with violent confrontations, but that does not make them less valuable.
They are entitled to determining their own level of safety.
Additionally, some protest groups may not want clearly marked medics because it draws unwanted attention from law enforcement.
You must figure out how you can provide support with or without other medics in a way that truly supports the spirit of the action you are attending.
\index{diversity of tactics|)}

\triplesubsection{Principle 2}
Maintaining a separation of time and space is used to isolate legal risk, risk of repression, and risk of violence faced by participants and other medics.
Your actions as an individual can affect all of these types of risk for individuals present.
Walking around in a full EMS uniform with protesters who are trying to blend in may draw attention they do not want.
Likewise, protesters will need to be aware of medical support and not drag them into confrontations they are not prepared to handle.

\index{legality|(}
\index{operational security|(}
This extends to planning and medics may not way to be directly involved in the planning of actions as they become a legal liability who can be interrogated about the actions.
Some medics tactically choose to maintain separation from activists to minimize the chances of information leaking.
\index{operational security|)}
\index{legality|)}

\index{police!tactics|(}
\triplesubsection{Principle 3}
A common police repression tactic is to foment internal conflict within organizations to break down solidarity.
For you as a medic this means avoiding criticizing individuals and groups, medical or otherwise, in a public way that can be used by police or media to show a divided movement.
Animosity between medics and protesters may lead to people asking the question ``Will they treat me or not?''
This should be avoided to the largest extend possible.
\index{police!tactics|)}

\index{snitches!St. Paul Principles@in St. Paul Principles|(}
\triplesubsection{Principle 4}
Simply put, all cops are bastards.\index{cops!all are bastards}
You may find it tactically advantageous to maintain a professional level of courtesy with local law enforcement to ease your job, but medics never provide information about comrades, organizations, or actions to the police or the State.
Because you cannot trust strangers to not pass on information, you should avoid discussing anything related to the movement or action with people who are not known, trusted comrades.
This includes medical professionals.
EMS does not need to be told in advanced where blockades may happen or what days to expect and influx of emergency department admissions.
\index{snitches!St. Paul Principles@in St. Paul Principles|)}

\index{St. Paul Principles|)}

\section*{Summary}

You may want to work on your own, or you may choose to join a collective.
Whatever you do, work with a buddy, and coordinate with others.
Do what you can to develop solidarity with those you work with, those you treat, and those in your community.
Remember: our solidarity is a weapon.

\index{organizing|)}
