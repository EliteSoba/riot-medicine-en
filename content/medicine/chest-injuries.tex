\chapter{Chest Injuries}
\label{ch:chest_injuries}

\epigraph{I think it is healing behavior, to look at something so broken and see the possibility and wholeness in it.}
{adrienne maree brown, \textit{Emergent Strategy}\supercite{emergent-strategy}}

\index{torso|seealso {chest}}
\index{torso|seealso {abdomen}}
\index{thorax|see {chest}}
\index{chest!trauma|(}
\index{trauma!chest|(}

\noindent
Aside from the brain, the most important organs in the human body are located in the chest.
Chest injuries can range from something easily managed like a minor rib fracture to more serious injuries that can lead to life-threatening interruptions to the cardiovascular and respiratory systems.
Possible causes of chest injuries medics will likely see are: being struck by a blunt object (such as a police baton), getting hit by a vehicle, being stabbed or shot, or falling from a height.

The physiology of the chest covered in \fullref{ch:basic_life_support} is recommended reading before starting this chapter.

\section*{Physiology}

The chest, also known as the thorax, is the part of body between the neck and the diaphragm.
The part of the torso below the diaphragm is the abdomen.
Structurally, the chest includes both the rib cage and the shoulder girdle.
The rib cage is the vertebral column, ribs, and sternum (breastbone).
The shoulder\index{shoulder girdle} girdle is the clavicles\index{clavicle} (collarbones) and scapulae (shoulder blades).
The chest also includes the muscles, skin, and connective tissues surrounding these structures.

\index{ribs!physiology@physiology of|(}
There are twelve pairs of ribs numbered according to which thoracic vertebra they attach to.
The first seven ribs are called true ribs and are connected to the sternum by cartilage.
The next five ribs are called false ribs.
The first three false ribs are connected to the sternum by a common cartilaginous connection.
The last two false ribs are ``floating ribs'' that are not connected to the sternum at all.
\index{ribs!physiology@physiology of|)}

\index{thoracic cavity|(}
The interior space of the chest is the interior space of the ribcage above the diaphragm.
This is called the thoracic cavity.
The thoracic cavity houses the heart, great blood vessels (aorta, vena cava, and their major branches), lungs\index{lungs}, bronchi, trachea, and esophagus. The thoracic cavity is protected by the thoracic wall which is made up of the ribs, muscle, and connective tissue.
The intercostal muscles\index{intercostal muscles} are the muscles of the thoracic wall connected to the ribs, both between and along the chest wall.
\index{thoracic cavity|)}

\index{plurae|(}
The pulmonary pleurae, normally referred to simply as pleurae, are the membranes that surround the lungs.
The parietal pleura is the outer pleura and is attached to the interior of the thoracic wall and covers the upper surface of the diaphragm.
The visceral pleura is the membrane that covers the outside of each lung.
Between the two pleurae is the fluid-filled pleural space.
\index{plurae|)}

Between the lungs is the mediastinum\index{mediastinum}, the central space of the thoracic cavity and houses the heart, esophagus, and trachea, among other structures.

\section*{Rib Fractures}
\index{fractures!rib|(}
\index{ribs!fractures@fractures of|(}

A rib fracture is a fracture to one of the ribs.
Rib fractures with a single break with no bone displacement can be called hairline or simple fractures.
Fractures are less common in ribs 11 and 12 as they are more flexible and will flex further before breaking.
Possible complications of a rib fracture include pneumothorax\index{pneumothoraces} and hemothorax\index{hemothoraces} which are discussed later in this chapter.
Patients may also develop pneumonia\index{pneumonia} due to their inability to breath deeply or cough.
Pneumonia develops slowly and is not of immediate concern for medics.

\subsection*{Signs and Symptoms}

A patient with a fractured rib will have pain at the site of the fracture with tenderness when it is palpated.
There may be bruising at the site of the fracture.
The patient may have chest pain that is sharp and stabbing when they breath in, cough, or sneeze.
They may have a shortness of breath and exhibit rapid, shallow breathing as they alter their breathing to compensate for the pain.

\subsection*{Treatment}

Treatment for a fractured rib is to immobilize the rib, manage pain, and consider evacuation to advanced medical care.

\triplesubsection{Immobilize the rib}
Pain can be managed by immobilizing the rib.
Taping, slinging, or both together can be used together to immobilize a fractured rib.

\quadruplesubsection{Tape the rib}
To tape a fracture, use 4 to 6 long strips of \SI{5}{\cm} medical tape to immobilize the rib.
The strips should be placed roughly perpendicular to the rib and stretch from the spine to the sternum. (\autoref{fig:taping_a_fractured_rib}).
Optionally, place a second set of tape perpendicular to the original to make a crosshatch pattern.
When taping, do not wrap fully around the chest as this may inhibit respiration.

\begin{figure}[htbp]
\centering
\caption{Taping a Fractured Rib\supercite{baedr}}
\label{fig:taping_a_fractured_rib}
\includesvg[height=5cm, keepaspectratio]{taping-rib}
\end{figure}

\quadruplesubsection{Sling the arm}
Stabilization of the fracture can also done by slinging the arm on the same side as the fracture.
This will help immobilize the arm and prevent movement of the ribs.
See \fullref{ch:fractures_and_dislocations} for instructions on how to fashion a sling.

\triplesubsection{Consider use of \acrshort{NSAID}s}
If you carry \acrshort{NSAID}s\\index{nonsteroidal anti-inflammatory drugs}, consider suggesting the patient self-administer following the guidelines in \fullref{ch:medication}.

\triplesubsection{Consider evacuation}
Simple rib fractures themselves do not require advanced medical care.
However, traumatic rib fractures may have concomitant injury to underlying viscera.
Patients who have fractures caused by high-energy impacts, with displacement, or with severe pain should be evacuated to advanced medical care.

\section*{Flail Chest}
\index{flail chest|(}
\index{chest!flail|(}

Flail chest, also called an unstable chest wall, is when multiple adjacent ribs have fractures with displacement in multiple places causing a segment of the thoracic wall to move independently of the rest of the chest (\autoref{fig:flail_chest}).
A large amount of force is necessary to cause an flail chest, so patients likely have other injuries, possibly including pulmonary contusions.

\begin{figure}[htbp]
\centering
\caption{Flail Chest\supercite{baedr}}
\label{fig:flail_chest}
\includesvg[width=\textwidth, height=6cm, keepaspectratio]{flail-chest}
\end{figure}

\index{paradoxical breathing!flail chest@in flail chest|(}

A key feature of flail chest is a phenomenon called paradoxical breathing (\autoref{fig:paradoxical_breathing}).
In a patient with an flail chest, the negative pressure during inspiration will cause the loose segment of the thoracic wall to move inwards while the rest of the chest expands.
Conversely, the positive pressure during expiration will cause the loose segment to move outwards while the rest of the chest contracts.

\begin{figure}[htbp]
\centering
\caption{Paradoxical Breathing\supercite{baedr}}
\label{fig:paradoxical_breathing}
\includesvg[width=\textwidth, keepaspectratio]{paradoxical-breathing}
\end{figure}

\index{paradoxical breathing!flail chest@in flail chest|)}

\subsection*{Signs and Symptoms}

A patient with flail chest will exhibit all the signs and symptoms of a fractured rib.
Additionally, they will have paradoxical breathing.
They may be in or develop respiratory distress.

To identify flail chest, place one hand under the patient's clothing over the suspected loose segment of the chest and feel for paradoxical movement while the patient breathes.
You may feel or hear crepitus.
The paradoxical movement may be noticeable on visual inspection.

\subsection*{Treatment}

Treatment is to stabilize the unstable segment of the chest wall, treat for shock, and evacuate the patient.

\triplesubsection{Stabilize the chest wall}
You will need to stabilize the loose segment of chest wall.
This can be done by taping a bulky dressing over the loose segment.
Do not wrap tape fully around the patient's chest as this can restrict their breathing.
The patient can also hold bulky article of clothing against the loose segment.

\triplesubsection{Position the patient comfortably}
While you are waiting for evacuation, have the patient find a comfortable position that minimizes pain at the fracture site.
This may be on their back or sitting up in a reclined position.
They may find it most comfortable to lay on their side with the fracture with a bulky piece of clothing held against the injury.

\triplesubsection{Treat for for shock}
The patient is at risk of shock due to pain and poor quality respirations.
Treat the patient for shock.

\triplesubsection{Consider administration of analgesics}
If evacuation will be prolonged or delayed, consider administering analgesics to reduce pain and make breathing more comfortable using the guidelines in \fullref{ch:medication}.

\triplesubsection{Evacuate}
Flail chest is a medical emergency.
Immediately evacuate the patient to advanced medical care.

\index{chest!flail|)}
\index{flail chest|)}
\index{fractures!rib|)}
\index{ribs!fractures@fractures of|)}

\section*{Lung Injuries}
\index{lungs!trauma|(}
\index{trauma!lungs|(}

Chest trauma may cause injury to the lungs or pleurae.
This may be caused by, among other things, fractured ribs tearing the pleurae or lungs or penetrating trauma.

The lungs may become bruised (pulmonary contusion\index{pulmonary contusions}) or they may become torn or cut (pulmonary laceration\index{pulmonary lacerations}).
Blunt force trauma may cause pulmonary contusions and, through the shear forces acting on the lungs, pulmonary lacerations.
Penetrating trauma can cause pulmonary lacerations.
The lungs are vulnerable to damage from explosion due to pressure waves (pulmonary barotrauma\index{barotrauma!pulmonary}), and patients in the vicinity of an explosion may have traumatic injuries to their lungs in the absence of visible, external trauma.
Damage to the lungs can inhibit gas exchange leading to shock.

\index{pneumothoraces|(}

A pneumothorax is the presence of air in the pleural space.
A pneumothorax can be either traumatic or spontaneous.
A spontaneous pneumothorax is caused by congenital weakness in the chest rupturing causing a pneumothorax.
A traumatic pneumothorax may be caused by penetrating injury or the tearing of the pleura by a fractured rib.
Traumatic pneumothoraces are categorized as open if there is passage of air from the outside environment into the pleural space, otherwise they are categorized as closed.
An open traumatic pneumothorax is colloquially called a ``sucking chest wound'' due to sucking sounds as air moves through the opening.
A hemothorax is the presence of blood in the pleural space.

A traumatic pneumothorax may create a one-way valve into the pleural space allowing air to enter the pleural space but not exit.
As air accumulates and the pressure increases, the lung will collapse.
Tissue may also bulge out from between the ribs.
This increasing pressure will push the heart and large blood vessels towards the unaffected side.
This is called a tension pneumothorax (\autoref{fig:tension_pneumothorax}).
As the pressure increases, pressure against the major veins decreases venous return and may cause obstructive shock.
If the pressure increases above that of the blood pressure in the veins, blood will not be able to return to the heart, and the patient will enter cardiac arrest\index{cardiac arrest!chest injuries@in chest injuries}.

\index{pneumothoraces|)}

\begin{figure}[tbp]
\centering
% TODO a doc commented that this pic implies that the chest on the affected side expands outward which is not true (tissue bulges through ribs, mediastinum moves toward unaffected side). The illustration should be updated in the next release and also show more obvious trachea deviation by including the outline of the shoulders/neck and possibly include a small arrow.
\caption{Tension Pneumothorax\supercite{baedr}}
\label{fig:tension_pneumothorax}
\includesvg[height=6cm, keepaspectratio]{tension-pneumothorax}
\end{figure}

\subsection*{Signs and Symptoms}

Signs and symptoms of lung injury are typically chest pain and shortness of breath.

\triplesubsection{Shortness of breath}
Patients with lung injuries will have a shortness of breath accompanied by rapid, and shallow breathing.
They may have chest pressure and may be in respiratory distress.
Poor gas exchange may result in cyanosis, especially of the lips.
This can be checked for by using a pulse oximeter.

\triplesubsection{Chest pain}
A spontaneous pneumothorax will present with sudden chest pain and a shortness of breath.
A traumatic pneumothorax may be harder to identify immediately as there may be other injuries that mask its presence.

\triplesubsection{Reduced signs of respiration}
Both pneumothoraces and hemothoraces may present with reduced movement of the chest wall on the affected side during respiration.
When using a stethoscope, the sounds of breathing may be reduced on the side with the injury.

\triplesubsection{Signs of shock}
Patients may go into shock due to blood loss into the pleural space, reduced gas exchange in the lungs, or obstructed blood flow to the heart.
Signs of shock such as pale, cool, clammy skin and sweating may be present.

\triplesubsection{Signs of tension pneumothorax}
If the patient has a tension pneumothorax, their trachea may deviate away from the affected side.
Their veins on their head and neck may become distended.
Tissue may started to bulge out from between the ribs.
The patient may develop hypotension.

\triplesubsection{Other signs}
The patient may be coughing up blood due to blood in the lungs and pleural space.

\subsection*{Treatment}

Treatment for lung injuries is to manage a possible tension pneumothorax, treat for shock, and evacuate the patient.

\triplesubsection{Consider related injuries}
If applicable, treat the patient for rib fractures and flail chest.

\triplesubsection{Lay patient on affected side}
If the patient has a pneumothorax or hemothorax, have the patient lay on their affected side.
Though painful, this will allow the unaffected lung to fully inflate thus delaying potentially lethal complications until they can be evacuated.

\index{dressings!occlusive|(}

\triplesubsection{Apply a chest vented seal}
If the patient has an open chest wound, you can minimize the severity of a pneumothorax by applying a chest vented seal.
If you do not have a commercial vented chest seal, an occlusive dressing can be improvised.

Note that a vented chest seal is specified.
A non-vented chest seal can trap air in the pleural space either causing or exacerbating a tension pneumothorax.

\quadruplesubsection{Commercial vented chest seal}
Commercial chest seals use extremely sticky adhesive that adheres to wet skin.
Be cautious of letting the seal come in contact with itself or your gloves.

Use trauma shears to cut away the patient's clothing.
Attempt to use gauze to dry to the skin around the wound to maximize adhesion between the skin and the chest seal.
Remove the seal from it's packaging.
Place the vent directly over the wound (\autoref{fig:application_vented_chest_seal}).
If the there is both an entrance and exit wound, place the vented seal on the \gls{anterior} wound to allow it to vent.
The posterior wound may use a non-vented chest steal so long as a vented seal was used on the \gls{anterior} wound.
Press the seal tight against the skin to ensure an airtight seal.

\begin{figure}[tbp]
\centering
\caption{Application of a Vented Chest Seal\supercite{baedr}}
\label{fig:application_vented_chest_seal}
\includesvg[width=6cm, keepaspectratio]{vented-chest-seal}
\end{figure}

\index{dressings!occlusive!improvised|(}
\quadruplesubsection{Improvised occlusive dressing}
A occlusive dressing can be improvised.
Use a square piece of a plastic bag to cover the wound.
Tape down three of the four sides to allow air to escape.
\index{dressings!occlusive!improvised|)}

\index{dressings!occlusive|)}

\index{decompression needles|(}
\triplesubsection{Avoid needle decompression}
Unless you are trained to do so, do not use a chest decompression needle to vent air from a tension pneumothorax.\footnotemark[1]
This can cause significant harm and complications.
\index{decompression needles|)}

\footnotetext[1]{
This is mentioned because some medics claim to carry chest decompression needles (\acrlong{TPAK}, \acrshort{TPAK}) in the event they encounter a pneumothorax.
The treatment described in this section can mitigate a pneumothorax sufficiently until the patient can be evacuated, and an invasive procedure done by an untrained medic is medically unethical.
The only reason you should carry a chest decompression needle is if you work with more qualified medics who are trained to use such a device.
}

\triplesubsection{Treat for shock}
Reduced ability to breath, blood loss, and reduced venous return may lead to shock.
Treat the patient for shock.

\triplesubsection{Evacuate}
Lung injuries can be a medical emergency.
\acrshort{SpO2} under 95\% requires monitoring the patient.
Immediately evacuate patients to advanced medical care if they are in respiratory distress, show signs of a pneumothorax, are coughing up blood, or have \acrshort{SpO2} under 90\%.

\index{trauma!lungs|)}
\index{lungs!trauma|)}

\section*{Summary}

Traumatic chest injury may be accompanied by rib fractures.
Fractures should be stabilized.
If multiple adjacent rib are fractured in multiple places, the patient may have flail chest and exhibit paradoxical breathing.
This is a sign of flail chest and requires immediate evacuation to advanced medical care.
Chest trauma of any sort, including explosions, can damage the lungs causing the patient to develop respiratory distress.
Patients who are coughing up blood of exhibit signs of pneumothorax need immediate evacuation to advanced medical care.
Patients with a traumatic chest injury who feel chest pressure, exhibit signs of respiratory distress, or have a reduced peripheral blood oxygen saturation need immediate evacuation to advanced medical care.

\index{trauma!chest|)}
\index{chest!trauma|)}
