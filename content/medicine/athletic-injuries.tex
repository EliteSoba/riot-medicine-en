\chapter{Athletic Injuries}
\label{ch:athletic_injuries}

\epigraph{
My girl gave me a bolt cutter / We love to break in / We claim all the spaces they forgot they had taken
}{P.O.S, \textit{Bolt Cutter}\supercite{doomtree-no-kings}}

\index{ligaments|seealso {sprains}}
\index{tendons|seealso {strains}}

\noindent
Athletic injuries are injuries to muscle, tendon, and ligament caused either by trauma or overuse.\footnotemark[1]
Often these are caused by during physical activities like running, climbing, and fighting, though they may be caused by accidents like tripping, falling, or simply lifting heavy objects.
These injuries are not life-threatening on their own, but if there is rioting or street fighting, individuals with athletic injuries may be incapacitated and unable to avoid danger.

\footnotetext[1]{
Collectively, injuries to the muscle, tendon, and ligament are called soft tissue injuries (\acrshort{STI}).
These include strains, sprains, contusions, and tendinitis.
Since this chapter focuses on acute and traumatic STIs, and since in this book contusions are covered by wound management, the term \acrshort{STI} is not used.
}

The physiology covered in \fullref{ch:fractures_and_dislocations} is considered a prerequisite for this chapter.

\section*{Physiology}

\index{ankles!physiology@physiology of|(}
Sprained ankles are the most common athletic injury during actions.
They are often related to falling or walking or running on uneven surfaces.
The anatomy of the ankle is complex (\autoref{fig:ankle_anatomy}), but it is approximately correctly to say that there is a ligament connecting each pair of adjacent bones\index{bones}.
Sprains are typically of the \acrlong{ATFL} (\acrshort{ATFL}) or the \acrlong{CFL} (\acrshort{CFL}).
Sprains of the deltoid ligament is rare due to its strength.
Large forces applied to the deltoid ligament are more likely to cause an avulsion fracture to the calcaneus (heel bone).
\index{ankles!physiology@physiology of|)}

\begin{figure}[htbp]
\caption{Anatomy of the Ankle\supercite{baedr}}
\label{fig:ankle_anatomy}
\centering
\begin{subfigure}[b]{5.8cm}
    \centering
	\includesvg[width=\textwidth, keepaspectratio]{ankle-lateral}
	\caption{\Gls{lateral} View}
\end{subfigure}
\begin{subfigure}[b]{5cm}
    \centering
	\includesvg[width=\textwidth, keepaspectratio]{ankle-medial}
    \vspace{0.15cm} % space to get the soles of the two feet even
	\caption{\Gls{medial} View}
\end{subfigure}
\end{figure}

\index{wrists!physiology@physiology of|(}
Sprains also are common in the wrist.
They tend to be related to falling or injuries while fighting.
Like the ankle, the anatomy of the wrist is complex (\autoref{fig:wrist_anatomy}), and it is again approximately correctly to say that there is a ligament connecting each pair of adjacent bones\index{bones}.
Sprains of the wrist tend to be radial rather than ulnar.
\index{wrists!physiology@physiology of|)}

\begin{figure}[htbp]
\caption{Anatomy of the Wrist\supercite{baedr}}
\label{fig:wrist_anatomy}
\centering
\begin{subfigure}[b]{5.3cm}
    \centering
	\includesvg[width=\textwidth, keepaspectratio]{wrist-dorsal}
	\caption{\Gls{dorsal} View}
\end{subfigure}
\begin{subfigure}[b]{5.3cm}
    \centering
	\includesvg[width=\textwidth, keepaspectratio]{wrist-palmar}
	\caption{\Gls{palmar} View}
\end{subfigure}
\end{figure}

\index{knees!physiology@physiology of|(}
The knee (\autoref{fig:knee_anatomy}) is another commonly sprained joint.
Medics who participate in or follow sports are likely familiar sprains of the \acrlong{ACL} (\acrshort{ACL}).
\index{knees!physiology@physiology of|)}

\begin{figure}[htbp]
\centering
\caption{Anatomy of the Knee\supercite{baedr}}
\label{fig:knee_anatomy}
\includesvg[height=6.5cm, keepaspectratio]{knee-anatomy}
\end{figure}

\section*{Sprains}
\index{sprains|(}

A sprain is an overstretched or torn ligament.
It is caused by either trauma or the joint moving beyond its functional range of motion.
Sprains may be mild and heal on their own within several days or they may involve a major rupture of the ligament requiring surgery.
The equivalent injury to a muscle or tendon is called a strain.

\subsection*{Signs and Symptoms}

Sprains are typically painful with swelling.
They may rapidly develop edema from broken blood vessels.
Bruising may occur shortly after the injury but may take several days to fully develop, and this bruising may not be localized to the injury.
For example, a sprained ankle can lead to bruising of the toes.
There may be an audible popping or cracking sound that accompanies the injury.
Depending on the severity of the sprain, there may be joint instability, loss of range of motion, and an inability to bear weight.

\index{sprains|)}

\section*{Strains}
\index{strains|(}

A strain is an overstretched or torn muscle, tendon, or both.
Strains, like sprains, may range from mild and quickly healing on their own to involving a major rupture and requiring surgery.
Strains can be caused by improper body mechanics such as when lifting heavy objects.
They can also be caused by overstretching a muscle such as when more force is applied against a muscle than the muscle can exert against that force.

\subsection*{Signs and Symptoms}

Strains are typically painful and have localized inflammation or bruising.
There may be loss of strength in affected muscle.

\index{strains|)}

\section*{Tendinitis}
\index{tendinitis|(}

Tendinitis is the inflammation of a tendon.
Tendinitis may be caused by disease, but in the case of riot medics, it is caused by an injury, repetitive use, or overuse.
Non-traumatic tendinitis is a progressive disease that typically develops over days or weeks, but there may be more rapid onset.
For example, a poorly fitted or overly tight boot can rub against the Achilles tendon and cause tendinitis by the end of a single action.

\subsection*{Signs and Symptoms}

Tendinitis typically presents with localized pain, redness, and warmth.
Palpation and exercise may exacerbate the pain.

\index{tendinitis|)}

\section*{Treatment for Athletic Injuries}
\index{strains!treatment@treatment of|(}
\index{sprains!treatment@treatment of|(}

Treatment for athletic injuries is to immobilize, facilitate healing, avoid further injury, and manage the pain.
The following methods can be used for immediate treatment, but they are not a substitute for surgery or physiotherapy.

\index{splinting|(}
\triplesubsection{Consider wrapping and splinting}
If you cannot differentiate an athletic injury from a fracture or dislocation, assume the worst and immobilize it.
Sprains that lead to an unstable joint, strains that lead to significant loss of function, or injuries with severe pain likely require advanced medical care.

Body parts can be wrapped to provide stability during evacuation and to protect them from further injury.
Wrapping can also help with tendinitis.
For example, wrapping a wrist can help minimize forearm tendinitis.
\index{splinting|)}

\index{nonsteroidal anti-inflammatory drugs|(}
\triplesubsection{Consider use of NSAIDs}
Use of NSAIDs can help reduce pain and swelling.
If you carry them, consider suggesting the patient self-administer following the guidelines in \fullref{ch:medication}.
\index{nonsteroidal anti-inflammatory drugs|)}

\index{PRICE method|(}
\triplesubsection{Use the PRICE method}
Treatment over the first 24 to 48 hours after injury used the \acrshort{PRICE} method to minimize pain and swelling.

\begin{table}[htbp]
\caption{PRICE Method}
\centering
\begin{tabular}[c]{c l}
    \hline
    \textbf{P} & Protection  \\
    \textbf{R} & Rest        \\
    \textbf{I} & Ice         \\
    \textbf{C} & Compression \\
    \textbf{E} & Elevation   \\
\end{tabular}
\end{table}

\quadruplesubsection{Protection}
The injured joint or muscle needs to be protected from further injury.
The extremity may need to be immobilized or padded during evacuation.

\quadruplesubsection{Rest}
The body needs rest to heal.
This may mean bed rest for a strained back muscle, or simply avoiding using the injured extremity.
A rule of thumb is that a patient should rest the injured body part until they can do simple activities without pain.

\quadruplesubsection{Ice}
Ice helps minimize swelling.
Whether or not this leads to faster healing is unclear, but it is useful for pain management.
Ice can be applied for a maximum of 20 minutes per hour, allowing the injury to warm naturally between applications.
Ice should not be placed directly against the skin.
A think towel or bandaged should be placed between the ice and the skin.
Excessive icing can lead to ischemia and frostbite.

\quadruplesubsection{Compression}
Use of an elastic bandage for compression helps reduce edema and provides some immobilization.
Use of inelastic bandages or tape for compression can impair blood flood.
The extremity should be periodically checked for sensation, circulation, and motion.

\quadruplesubsection{Elevation}
Elevating an injured limb above the heart helps reduce edema and associated pain.

\index{PRICE method|)}

\triplesubsection{Use the No HARM method}
Along with the things pains should do to help with healing, there are also things they should avoid for the first 72 hours.
The No \acrshort{HARM} method is avoiding heat, alcohol, re-injury, and massage.

\begin{table}[htbp]
\caption{No HARM Method}
\centering
\begin{tabular}[c]{c l}
    \hline
    \textbf{H} & Heat      \\
    \textbf{A} & Alcohol   \\
    \textbf{R} & Re-injury \\
    \textbf{M} & Massage   \\
\end{tabular}
\end{table}

\quadruplesubsection{No heat}
Application of hot packs or submersion in a hot bath should be avoided.
Increased blood flow as a result of heat leads to increased swelling.

\quadruplesubsection{No alcohol}
Consuming alcohol can increase blood flow leading to increased swelling. % TODO source?
More importantly, it can decrease sensitivity to the injury leading to aggravation of the injury.

\quadruplesubsection{No re-injury}
Related to resting, engaging in activities that could re-injure the body part should be avoided.

\quadruplesubsection{No massage}
Avoid massages as they may cause additional tissue damage.

\triplesubsection{Consider evacuation}
Resting and avoiding re-injury may mean that patients need to leave the action.
A sprained wrist at a peaceful action does not require evacuation.
At an action where there is expected confrontation with the State of fascists, patients with athletic injuries may need to be evacuated.
Their injuries may prevent them from running from danger or fighting to protect themself.
Recommendations for evacuation should be done at the medic's discretion.

\triplesubsection{Recommend light exercise}
After the first 24 to 48 hours, light exercise helps promote healing.
Ideally, the patient should seek physiotherapy, but in the absence of that, a general rule is to do light exercises that promote strength in affected body part.
Recommend this to the patient.

\subsection*{Wrapping Techniques}

The following wrapping techniques can be used to provide support.
A wrap should be snug (but not tight), provide support, and somewhat immobilize the joint.
All wraps should be done with an elastic bandage or self-adhering bandage.

\subsubsection*{Wrapping an Ankle}
\index{ankles|(}

When wrapping an ankle (\autoref{fig:ankle_wrap}), the foot should be at a 90 degree angle to the leg.
Start the wrap on the bridge of the foot, then make figure 8's around the ankle.
Finish the wrap by wrapping around the Achilles tendon.

Depending on the length of your bandages, you may not be able to wrap the Achilles tendon.
The most important part of the wrap is the figure 8's to stabilize the ankle.

\begin{figure}[htbp]
\caption{Wrapping Joints\supercite{baedr}}
\centering
\begin{subfigure}[b]{5.3cm}
    \centering
	\includesvg[width=\textwidth, height=6cm, keepaspectratio]{ankle-wrap}
	\caption{Ankle Wrap}
    \label{fig:ankle_wrap}
\end{subfigure}
\begin{subfigure}[b]{5.3cm}
    \centering
	\includesvg[width=\textwidth, height=6cm, keepaspectratio]{wrist-wrap}
    \caption{Wrist Wrap}
    \label{fig:wrist_wrap}
\end{subfigure}
\begin{subfigure}[b]{0.3\textwidth}
    \centering
	\includesvg[width=\textwidth, height=6cm, keepaspectratio]{knee-wrap}
    \caption{Knee Wrap}
    \label{fig:knee_wrap}
\end{subfigure}
\end{figure}

\index{ankles|)}

\subsubsection*{Wrapping a Wrist}
\index{wrists|(}

When wrapping a wrist (\autoref{fig:wrist_wrap}), the hand should be open, the fingers spread, and the wrist in line with the forearm.
Start the wrap on the forearm, then make diagonal wraps up the hand.
One wrap should fully wrap the palm of the hand to serve as an anchor.
Make additional diagonal wraps back toward the wrist.
Finish by securing the wrap on the wrist.

\index{wrists|)}

\subsubsection*{Wrapping a Knee}
\index{knees|(}

When wrapping a knee (\autoref{fig:knee_wrap}), all wraps should be as close to the joint as possible.
Because of the tapered shape of the quadriceps and calf muscles, wraps that are too far from the joint will slide back toward the joint and become loose.
Start the wrap above the knee making two circles to act as an anchor.
Make a diagonal wraps, crossing between the upper and lower leg on the back of the knee.

\index{knees|)}
\index{sprains!treatment@treatment of|)}
\index{strains!treatment@treatment of|)}

\section*{Summary}

Athletic injuries are relatively easily managed without the need for advanced medical care, but they are debilitating enough to prevent patients from engaging in physical activity.
Typically all that is needed is to wrap the injury and send the patient home.
Patients should be instructed to be wary of lack of healing and a worsening state of the injury as this may be a sign of a fracture.
When an athletic injury cannot be differentiated from a fracture, be conservative and treat it like a fracture.
Discourage patients with athletic injuries from staying at actions where they may try to fight as this can cause severe re-injury.
