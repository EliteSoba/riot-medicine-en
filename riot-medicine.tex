\documentclass[a5paper,twoside,openany]{book}

\usepackage[utf8]{inputenc}

% need to import before other packages to prevent options clash
\usepackage[hidelinks]{hyperref}

\usepackage[
    alldates=edtf, % ISO-8601 dates in bibliography
    sorting=none,
    style=numeric,
    citestyle=numeric-comp,
]{biblatex}
\usepackage{bookmark} % to get \bookmarksetup
\usepackage{csquotes}
\usepackage{enumitem} % to remove padding on items in lists
\usepackage{epigraph}
\usepackage[
    a5paper,
    total={110mm,170mm},
    left=20mm,
    top=20mm,
]{geometry}
\usepackage[acronym,nopostdot,toc]{glossaries}
\usepackage{graphicx}
\usepackage{imakeidx}
\usepackage[
    columns=2,
    columnsep=20pt,
    indentunit=10pt,
    font=footnotesize,
    totoc=true,
]{idxlayout} % to decrease the font size
\usepackage[xcolor]{mdframed}
\usepackage{microtype} % for kerning/spacing/hyphenation
\usepackage{multirow}
\usepackage{numprint}
\usepackage{pgfplots} % for charts
\usepackage{siunitx}
\usepackage{subcaption}
\usepackage{svg}
\usepackage{tabularx}
\usepackage{titlesec} % for resizing the spacing on sections
\usepackage{titleps}
\usepackage{titling}
\usepackage{tikz}
\usepackage{verbatimbox} % for \addvbuffer
\usepackage{xifthen} % for \ifthenelse and \isempty

\input{hyphenation} % hyphenations for words not in the standard dictionary

\hypersetup{breaklinks=true}

\addbibresource{references.bib}
\renewcommand*{\bibfont}{\footnotesize} % decrease the size of the text in the bibliography

\renewcommand{\contentsname}{Table of Contents} % Rename the TOC
\renewcommand{\thefootnote}{\roman{footnote}} % set footnotes to use Roman numerals

\setlength{\epigraphwidth}{0.5\textwidth} % make quotes at start of chapters wider

\graphicspath{{./images/binary/}} % set search path for binary images
\svgpath{{./images/svg}} % set search path for SVG images

\setlist{noitemsep} % remove vertical space between items in itemize/enumerate

\npthousandsep{,} % for displaying large numbers

% command for subtitle on title page
\newcommand{\subtitle}[1]{
    \posttitle{
        \par\end{center}
        \begin{center}\large#1\end{center}
        \vskip0.5em
    }
}

% TODO this could be prettier, but it saves pages for now
\titleformat{\chapter}[hang]{\huge\bfseries}{\thechapter\ :\ }{0.ex}{\huge\bfseries\raggedright}
\titlespacing*{\chapter}{0ex}{1ex}{2ex plus 1ex minus 0.5ex}
\titlespacing*{\section}{0ex}{2.5ex plus 0.5ex minus 0.2ex}{.2ex plus 0.2ex minus 0.1ex}
\titlespacing*{\subsection}{0ex}{1.5ex plus 0.5ex minus 0.2ex}{.2ex plus 0.2ex minus 0.1ex}
\titlespacing*{\subsubsection}{0ex}{1ex plus 0.5ex minus 0.2ex}{.2ex plus 0.2ex minus 0.1ex}

\newcommand{\triplesubsection}[1]{\textbf{#1.} }
\newcommand{\quadruplesubsection}[1]{\underline{\textit{#1.}} }

% capitalize the autorefs
\renewcommand{\partautorefname}{Part}
\renewcommand{\chapterautorefname}{Chapter}
\renewcommand{\tableautorefname}{Table}
\renewcommand{\figureautorefname}{Figure}

% TODO this can overflow into the margins
\newcommand{\fullref}[1]{\hyperref[{#1}]{\autoref*{#1} (``\nameref*{#1}'')}}

% environment for "war stories"
% TODO remove left indentation on title
% TODO make author a subtitle?
\newenvironment{war-story}[2]
{
    \begin{mdframed}[
        frametitle={
            \ifthenelse{\isempty{#1}}{}{#1\ifthenelse{\isempty{#2}}{}{ - #2}}
        },
        backgroundcolor=lightgray!10,
        skipabove=0.7cm,
        skipbelow=0.7cm,
        innertopmargin=0.3cm,
        innerbottommargin=0.3cm,
    ]
}
{\end{mdframed}}

\newpagestyle{main}{
  %\setheadrule{.4pt}% Header rule
  \sethead[]% odd-left
          []% odd-center
          [\textit{\chaptertitle}\hskip 1em\thepage]% odd-right
          {\thepage\hskip 1em\textit{Riot Medicine}}% even-left
          {}% even-center
          {}% even-right
}

\makeindex[
    columns=2,
    columnsep=15pt,
    columnseprule,
    intoc=true,
]

\glsdisablehyper % disable hyperrefs for glossary terms because they are visual clutter
\makeglossaries
\input{acronyms}
\input{glossary}

\title{Riot Medicine}
\subtitle{First Edition\IfFileExists{git-commit}{ (\input{git-commit}\unskip)}{}}
\author{Håkan Geijer}
\date{2020-05-12}

\begin{document}
\inputencoding{utf8}
\renewcommand{\bibname}{References}

\frontmatter
% TODO make the title bigger, prettier, add logo
\maketitle

\include{content/copyright}
\include{content/disclaimer}
\include{content/acknowledgements}
\input{content/dedication}

% start the header styling here to prevent weird styling in the front matter pages
\cleardoublepage % have to double clear because latex is somehow stupid and otherwise left/right and even/odd get messed up
\pagestyle{main}

\setcounter{tocdepth}{0} % only show parts and chapters (not sections, subsections, etc.)
\tableofcontents

\include{content/intro}

\mainmatter

\part{Organize!}
\include{content/organize/responsibilities}
\include{content/organize/organizational-structures}
\include{content/organize/pre-action}
\include{content/organize/post-action}
\include{content/organize/training}

\part{Medicine}
\include{content/medicine/patient-assessment}
\include{content/medicine/patient-evacuation}
\include{content/medicine/psychological-care}
\include{content/medicine/medication}
\include{content/medicine/alternative-medicine}
\include{content/medicine/basic-life-support}
\include{content/medicine/wound-management}
\include{content/medicine/riot-control-agent-contamination}
\include{content/medicine/shock}
\include{content/medicine/brain-and-spinal-cord-injuries}
\include{content/medicine/chest-injuries}
\include{content/medicine/fractures-and-dislocations}
\include{content/medicine/athletic-injuries}
\include{content/medicine/burns}
\include{content/medicine/combat-injuries}
\include{content/medicine/cold-injuries}
\include{content/medicine/heat-illnesses}
\include{content/medicine/respiratory-and-cardiac}
\include{content/medicine/allergies-and-anaphylaxis}
\include{content/medicine/blood-glucose-disorders}
\include{content/medicine/drug-overdoses}
\include{content/medicine/unresponsive-states}

\part{Equipment}
\include{content/equipment/medical-supplies}
\include{content/equipment/ppe}
\include{content/equipment/other-gear}
\include{content/equipment/packing}

\part{Tactics}
\label{pt:tactics}
\include{content/tactics/general-tactics}
\include{content/tactics/action-tactics}
\include{content/tactics/opsec}
\include{content/tactics/radio-operation}

\backmatter
\bookmarksetup{startatroot} % to prevent the afterword (and following chapters) from being part of the previous Part

\include{content/afterword}
\include{content/further-reading}

\printglossary[type=\acronymtype,nonumberlist]
\printglossary[nonumberlist]

\clearpage
\printindex

\clearpage
\phantomsection\addcontentsline{toc}{chapter}{\bibname} % add the references to the TOC
\defbibnote{bib-prenote}{Remember, folx: paying for knowledge is bullshit.
Pirate papers from \url{https://sci-hub.tw/}.
Download books from \url{https://libgen.lc/}.
Information wants to be free.
}
\printbibliography[prenote=bib-prenote]

\end{document}
